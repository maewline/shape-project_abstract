/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.shape;

/**
 *
 * @author Arthit
 */
public class Square extends Shape {
    private double side;

    public Square(double side) {
        super("Square");
        this.side=side;
    }

    @Override
    public double calArea() {
        if(side==0){
            System.out.println("Side must more than 0 !!!");
        }return side*side;
    }

    @Override
    public String toString() {
        return "Square " + "Side  = " + side+" Area is = "+this.calArea();
    }
    
    
    
}
