/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.shape;

/**
 *
 * @author Arthit
 */
public class Circle extends Shape{
    private final double pi =22.0/7;
    private double r;

    public Circle(double r) {
        super("Circle");
        this.r = r;
    }

    @Override
    public double calArea() {
        if(r==0){
            System.out.println("Radius much more than 0 !!!");
        }return pi*r*r;
    }
    
    @Override
    public String toString() {
        return "Circle " + "Radius = " + this.r +" Area is = "+this.calArea();
    }
    
}
