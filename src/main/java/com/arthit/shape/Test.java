/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.shape;

/**
 *
 * @author Arthit
 */
public class Test {
    public static void main(String[] args) {
        Rectangle rec1 = new Rectangle(3,2);
        Rectangle rec2 = new Rectangle(4,3);
        rec1.calArea();
        System.out.println(rec1);
        rec2.calArea();
        System.out.println(rec2);
        Enter();
        
        Square sq1 = new Square(4);
        Square sq2 = new Square(2);
        sq1.calArea();
        System.out.println(sq1);
        sq2.calArea();
        System.out.println(sq2);
        Enter();
        
        Circle cir1 = new Circle (1.5);
        cir1.calArea();
        System.out.println(cir1);
        Circle cir2 = new Circle (4.5);
        cir2.calArea();
        System.out.println(cir2);
         Circle cir3 = new Circle (5.2);
        cir3.calArea();
        System.out.println(cir3);
        Enter();
        
        Shape [] shapes = {rec1,rec2,sq1,sq2,cir1,cir2,cir3};
        for(int i =0;i<shapes.length;i++){
            System.out.println(shapes[i].getName()+" is Area = "+shapes[i].calArea());
        }
        
    }
    
    

    private static void Enter() {
        System.out.println();
    }
}
