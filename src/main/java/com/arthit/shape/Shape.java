/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.shape;

/**
 *
 * @author Arthit
 */
public abstract class Shape {
    protected String name;

    public String getName() {
        return name;
    }

   

    public Shape(String name) {
        this.name = name;
    }
    
    public abstract double  calArea();
}
